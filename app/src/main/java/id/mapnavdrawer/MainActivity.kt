package id.mapnavdrawer

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import id.mapnavdrawer.response.Response
import id.mapnavdrawer.utils.DirectionMapsV2
import id.mapnavdrawer.utils.GPSTrackers
import id.mapnavdrawer.utils.HeroHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback, PlaceSelectionListener {
    private var mMap: GoogleMap? = null
    var gps: GPSTrackers? = null
    var lat: Double? = null
    var lng: Double? = null

    var textViewJarak: TextView? = null
    var textViewTarif: TextView? = null
    var textViewDurasi: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        textViewTarif = findViewById(R.id.text_view_tarif) as TextView
        textViewJarak = findViewById(R.id.text_view_jarak) as TextView
        textViewDurasi = findViewById(R.id.text_view_duration) as TextView

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val fab = findViewById(R.id.fab) as FloatingActionButton
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
        fab.visibility = View.GONE

        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)


        var placeAutoComplete = fragmentManager.findFragmentById(R.id.place_autocomplete_fragment) as PlaceAutocompleteFragment
        placeAutoComplete.setOnPlaceSelectedListener(this);
        placeAutoComplete.setHint("Search Location");
    }

    override fun onBackPressed() {
        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        var style = MapStyleOptions.loadRawResourceStyle(this, R.raw.style)
        mMap!!.setMapStyle(style)
        // Add a marker in Sydney and move the camera
        val sydney = LatLng(-6.1954112, 106.7946166)

        /*   val loc = LatLng(-6.1925297, 106.8001397)
           mMap!!.addMarker(MarkerOptions()
                   .position(loc).title("PN Jakarta Barat")
                   .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_round)))

           val loc2 = LatLng(-6.1912513, 106.7896575)
           mMap!!.addMarker(MarkerOptions()
                   .position(loc2).title("Pasar Slipi Jaya")
                   .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_round)))*/

        val cameraPosition = CameraPosition.Builder()
                .target(sydney).zoom(15f).build()

        mMap!!.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition))

        mMap!!.uiSettings.isCompassEnabled = true
        mMap!!.uiSettings.isRotateGesturesEnabled = true
        mMap!!.uiSettings.isZoomControlsEnabled = true
        mMap!!.uiSettings.isZoomGesturesEnabled = true

        /* mMap!!.uiSettings.isMyLocationButtonEnabled = true
         mMap!!.isMyLocationEnabled = true*/
        gps = GPSTrackers(this)
        if (gps?.canGetLocation!!) {
            lat = gps?.getLatitude()
            lng = gps?.getLongitude()

            mMap!!.addMarker(MarkerOptions()
                    .position(LatLng(lat!!, lng!!)).title("Current Location"))
        } else {
            Toast.makeText(this, "Can't get location", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onPlaceSelected(p0: Place?) {
        Toast.makeText(this, p0?.name, Toast.LENGTH_SHORT).show()
        val loc2 = p0?.latLng
        mMap!!.addMarker(MarkerOptions()
                .position(loc2!!).title(p0?.name.toString())
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_round)))
        getRoute(p0?.latLng.latitude, p0?.latLng.longitude)
    }

    override fun onError(p0: Status?) {
        Toast.makeText(this, p0.toString(), Toast.LENGTH_SHORT).show()
    }


    fun getRoute(s: Double, ss: Double) {
        //init retrofit
        var retrofit = Retrofit.Builder().baseUrl("https://maps.googleapis.com/maps/api/directions/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        var req = retrofit.create(APIService::class.java)
        var call = req.req("" + lat + "," + lng, "" + s + "," + ss)
        call.enqueue(object : Callback<Response> {
            override fun onResponse(call: Call<Response>?, response: retrofit2.Response<Response>?) {
                if (response?.isSuccessful!!) {
                    var route = response.body()?.routes
                    var obj0 = route?.get(0)
                    var overview = obj0?.overviewPolyline
                    var point = overview?.points

                    var direction = DirectionMapsV2(this@MainActivity)
                    direction.gambarRoute(mMap!!, point!!)

                    var obj0Leg = obj0?.legs
                    var distance = obj0Leg?.get(0)?.distance?.text
                    var duration = obj0Leg?.get(0)?.duration?.text

                    textViewJarak?.text = "Jarak : " + distance
                    val tarif = 5500
                    val total = distance.toString().replace(" km", "").toDouble() * tarif

                    textViewDurasi?.text = "Durasi : " + duration
                    textViewTarif?.text = "Tarif : Rp " + HeroHelper.toRupiahFormat2(total.toString())

                }
            }

            override fun onFailure(call: Call<Response>?, t: Throwable?) {

            }

        })

    }
}
