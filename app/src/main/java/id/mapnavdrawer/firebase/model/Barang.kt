package id.mapnavdrawer.firebase.model

/**
 * Created by Andini Rachmah on 9/22/2017.
 */
class Barang {
    var nama: String? = null
    var harga: String? = null
    var key: String? = null

    constructor(nama: String?, harga: String?) {
        this.nama = nama
        this.harga = harga
    }

    constructor()
    constructor(nama: String?, harga: String?, key: String?) {
        this.nama = nama
        this.harga = harga
        this.key = key
    }

    override fun toString(): String {
        return nama.toString()
    }

}