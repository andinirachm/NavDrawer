package id.mapnavdrawer

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.location.places.ui.PlaceSelectionListener

class PlacePickerActivity : AppCompatActivity(), PlaceSelectionListener {
    override fun onPlaceSelected(p0: Place?) {
        Toast.makeText(this, p0?.name, Toast.LENGTH_SHORT).show()

    }

    override fun onError(p0: Status?) {
        Toast.makeText(this, p0.toString(), Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place_picker)

        var placeAutoComplete = fragmentManager.findFragmentById(R.id.place_autocomplete_fragment) as PlaceAutocompleteFragment
        placeAutoComplete.setOnPlaceSelectedListener(this);
        placeAutoComplete.setHint("Search Location");
    }
}
