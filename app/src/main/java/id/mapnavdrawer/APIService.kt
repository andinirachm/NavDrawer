package id.mapnavdrawer

import id.mapnavdrawer.response.Response
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Andini Rachmah on 9/20/2017.
 */
interface APIService {
    @GET("json")
    fun req(@Query("origin") origin: String, @Query("destination") dest: String): Call<Response>

}