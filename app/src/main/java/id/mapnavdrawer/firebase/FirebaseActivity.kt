package id.mapnavdrawer.firebase

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.firebase.database.*
import id.mapnavdrawer.R
import id.mapnavdrawer.firebase.model.Barang
import kotlinx.android.synthetic.main.activity_firebase.*

class FirebaseActivity : AppCompatActivity() {
    var arrStr: ArrayList<String>? = ArrayList()
    var arrBarang: ArrayList<Barang>? = ArrayList()
    var firebase: FirebaseDatabase? = null
    var db: DatabaseReference? = null
    var keyq: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firebase)

        firebase = FirebaseDatabase.getInstance()
        db = firebase?.getReference("barang")
        db?.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(p0: DataSnapshot?) {
                arrStr?.clear()
                arrBarang?.clear()
                if (p0 != null) {
                    for (item in p0.getChildren()) {
                        var key = item.key
                        var name = item.child("nama").value
                        var harga = item.child("harga").value
                        arrStr?.add(name.toString())
                        arrBarang?.add(Barang(name.toString(), harga.toString(), key.toString()))
                    }

                    var adapter = ArrayAdapter(this@FirebaseActivity, android.R.layout.simple_list_item_1, arrBarang)
                    list_view.adapter = adapter
                } else {
                    Log.d(localClassName, "SIZE : data null")
                }
            }
        })

        list_view.setOnItemClickListener { adapterView, view, position, l ->
            var barang = arrBarang?.get(position)
            edit_text_nama.setText(barang?.nama)
            edit_text_harga.setText(barang?.harga)
            keyq = barang?.key.toString()
            Toast.makeText(this, keyq, Toast.LENGTH_SHORT).show()
        }

        btn_update.setOnClickListener {
            if (edit_text_nama.text.toString().isNotEmpty() && edit_text_harga.text.toString().isNotEmpty()) {
                update(keyq!!)
            } else {
                Toast.makeText(this, "Nama dan harga harus diisi", Toast.LENGTH_SHORT).show()
            }
        }

        btn_add.setOnClickListener {
            var key = db?.push()?.key // json array
            var barang = Barang(edit_text_nama.text.toString(), edit_text_harga.text.toString())
            db?.child(key)?.setValue(barang)
        }
    }

    fun update(key: String) {
        db?.child(key)?.child("nama")?.setValue(edit_text_nama.text.toString())
        db?.child(key)?.child("harga")?.setValue(edit_text_harga.text.toString())
    }

    fun remove(key: String) {
        db?.child(key)?.setValue(null)
    }
}

